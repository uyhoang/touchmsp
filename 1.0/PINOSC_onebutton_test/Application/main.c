#include "CTS_Layer.h"
#include <stdbool.h>
#include "main.h"
#define DELAY 500 		// Timer delay timeout count - 5000*0.1msec = 500 msec
//unsigned int dCnt;
#define NUM_SEN 3
#define LEFT BIT0
#define MIDDLE BIT1
#define RIGHT BIT2
uint16_t dCnt[NUM_SEN];
struct Element* keypressed;
struct Element* prev_keypressed;
const struct Element* address_list[NUM_SEN] =
{
    &left_element,
    &middle_element,
    &right_element,
};
uint8_t curr_key_state, prev_key_state, led_state, state_send;
bool ischangestate, istouched, isreceive;
unsigned int i = 0;
uint8_t predata,data,reg=1,ui8char='A';
// Sleep Function
// Configures Timer A to run off ACLK, count in UP mode, places the CPU in LPM3 
// and enables the interrupt vector to jump to ISR upon timeout 
void sleep(unsigned int time)
{
    TA0CCR0 = time;
    TA0CTL = TASSEL_1+MC_1+TACLR;
    TA0CCTL0 &= ~CCIFG;  //xoa co interrupt
    TA0CCTL0 |= CCIE; 
    __bis_SR_register(LPM3_bits+GIE);
    __no_operation();
}
void change_led_state(uint8_t *pled_state, uint8_t led)
{
    *pled_state^=led;
}
void debug_led(char num)
{
    if (num&0x01)
        P3OUT|=LED1B;
    else
        P3OUT&=~LED1B;
    if (num&0x02)
        P3OUT|=LED2B;
    else
        P3OUT&=~LED2B;
    if (num&0x04)
        P1OUT|=LED3B;
        else
            P1OUT&=~LED3B;
}
void led_state_to_num()
{
   state_send=led_state+48;
}
// Main Function
void main(void)
{ 

  // Initialize System Clocks
  WDTCTL = WDTPW + WDTHOLD;             // Stop watchdog timer
  BCSCTL1 = CALBC1_1MHZ;                // Set DCO to 1, 8, 12 or 16MHz
  DCOCTL = CALDCO_1MHZ;
  BCSCTL2 |= DIVS_2;                    // divide SMCLK by 4 for 250khz
  BCSCTL3 |= LFXT1S_2;                  // LFXT1 = VLO
  P1OUT = 0x00;							// Clear Port 1 bits
  P1DIR |= LED3B;
  //P3SEL
  P3DIR |= LED1B+LED2B+LED1A+LED2A+LED3A+RL2+RL1;
  P3OUT &= ~(LED1B+LED2B);
  P2DIR |= RL3;
  ischangestate=1;
  nrf24l01_pin_init();
  nrf24l01_set_as_rx(3);
 // __delay_cycles(130);
  nrf24l01_init();
  nrf24l01_set_as_rx(3);

  TI_CAPT_Init_Baseline(&three_button);
   // Update baseline measurement (Average 5 measurements)
  TI_CAPT_Update_Baseline(&three_button,5);
  // Main loop starts here
  while (1)
  {
    prev_keypressed = keypressed;
	keypressed = (struct Element *)TI_CAPT_Buttons(&three_button);
	if(keypressed!=prev_keypressed)
	{
	    prev_key_state = curr_key_state;
	    curr_key_state=0;
	    if (keypressed==address_list[0])
	        curr_key_state |= LEFT;
	    else if (keypressed==address_list[1])
	        curr_key_state |= MIDDLE;
	    else if (keypressed==address_list[2])
	        curr_key_state |= RIGHT;
	    istouched=1;
	}

	if(istouched)
	{
	    istouched=0;
	    if (curr_key_state&LEFT)
	        change_led_state(&led_state,LEFT);
        if (curr_key_state&MIDDLE)
            change_led_state(&led_state,MIDDLE);
        if (curr_key_state&RIGHT)
            change_led_state(&led_state,RIGHT);
        ischangestate=1;
	}
	if(~nrf24l01_irq_status())
	{
	if (nrf24l01_irq_rx_dr()!=0)
        {
	        ischangestate=1;
            nrf24l01_irq_clear_all();
            nrf24l01_read_rx_payload(&data, 1);
            switch (data)
            {
            case 'a':
            {
                change_led_state(&led_state,LEFT);
                break;
            }
            case 'b':
            {
                change_led_state(&led_state,MIDDLE);
                break;
            }
            case 'c':
            {
                change_led_state(&led_state,RIGHT);
                break;
            }
            nrf24l01_flush_rx_fifo();
            }
        }
	}
     if(ischangestate)
     {
        if (led_state&LEFT)
        {
          P1OUT |= LED3B;
          P3OUT |= LED3A;
            P2OUT |= RL3;
        }
        else
        {
          P1OUT &=~ LED3B;
          P3OUT &=~ LED3A;
          P2OUT &=~ RL3;
        }
        if (led_state&MIDDLE)
        {
          P3OUT |= (LED2A+LED2B);
          P3OUT |= RL2;
        }
        else
        {
          P3OUT &=~ (LED2A+LED2B);
          P3OUT &=~ RL2;
        }
        if (led_state&RIGHT)
        {
          P3OUT |= (LED1B+LED1A);
          P3OUT |= RL1;
        }
        else
        {
          P3OUT &=~ (LED1B+LED1A);
          P3OUT &=~ RL1;
        }
        led_state_to_num();
        nrf24l01_set_as_rx(0); //Set as TX
        nrf24l01_write_tx_payload(&state_send,1);
        nrf24l01_transmit();
 //       if(nrf24l01_irq_tx_ds()!=0)
        nrf24l01_irq_clear_all();
        nrf24l01_flush_tx_fifo();
        nrf24l01_set_as_rx(1); //Set as RX
        ischangestate=0;
	}
    sleep(DELAY);
  }
} // End Main

/******************************************************************************/
// Timer0_A0 Interrupt Service Routine: Disables the timer and exists LPM3  
/******************************************************************************/
#pragma vector=TIMER0_A0_VECTOR
__interrupt void ISR_Timer0_A0(void)
{
  TA0CTL &= ~(MC_1);
  TA0CCTL0 &= ~(CCIE);
  __bic_SR_register_on_exit(LPM3_bits+GIE);
}
#pragma vector=PORT2_VECTOR,             \
  PORT1_VECTOR,                            \
  USI_VECTOR,                            \
  NMI_VECTOR,COMPARATORA_VECTOR,         \
  ADC10_VECTOR
__interrupt void ISR_trap(void)
{
  // the following will cause an access violation which results in a PUC reset
  WDTCTL = 0;
}
#pragma vector=TIMER0_A1_VECTOR
__interrupt void Timer0_A1_ISR(void)
{
    WDTCTL = 0;
}
