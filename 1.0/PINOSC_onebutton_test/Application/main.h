/*
 * main.h
 *
 *  Created on: Apr 8, 2017
 *      Author: hp
 */
#include <msp430.h>
#include "Basic_config.h"
#include "nrf.h"
#include <stdint.h>
#ifndef MAIN_H_
#define MAIN_H_

#define PAD1 BIT3 //PORT2
#define PAD2 BIT5 //PORT2
#define PAD3 BIT7 //PORT1
#define LED1A BIT2 //PORT3
#define LED1B BIT3 //PORT3
#define LED2A BIT6 //PORT3
#define LED2B BIT5 //PORT3
#define LED3A BIT7 //PORT3
#define LED3B BIT6 //PORT1
#define RL1 BIT0 //PORT3
#define RL2 BIT1 //PORT3
#define RL3 BIT0 //PORT2
#endif /* MAIN_H_ */
